"""
Convert huge xml file from myanimelist to cute txt file
"""


from os import path #: find the script dir's abspath
from difflib import SequenceMatcher #: longest substring
import xml.etree.ElementTree as ET #: for xml parsing

HERE = path.abspath(path.dirname(__file__))

def xml_to_dict():
    """
    Seqv xml data to 2 dicts
    """
    tree = ET.parse(path.join(HERE, 'anilistxml.xml'))
    root = tree.getroot()
    anidict = {} ##standalone-titles -> all titles
    adddict = {} #franchise titles -> standalone-titles
    for one in root.findall('anime'):
        series_title = one.find('series_title').text
        series_episodes = one.find('series_episodes').text
        my_score = one.find('my_score').text
        my_status = one.find('my_status').text
        my_comments = one.find('my_comments').text
        if my_status == 'Completed':
            if my_comments is None: #standalone's title mark
                anidict[series_title] = [series_episodes, my_score]
            else:
                adddict[series_title] = [series_episodes, my_score, my_comments]
    return anidict, adddict

def list_seqven(addlist):
    """
    Sum fran stuff to one
    """
    first_tit = addlist.pop() # there should always be at least 2 els to pop()
    unify_tit = first_tit[0]
    sco = first_tit[1][1] # it's always the same
    ser = int(first_tit[1][0])
    for one in addlist.copy():
        second_tit = addlist.pop()
        match = (SequenceMatcher(None, unify_tit, second_tit[0]).
                 find_longest_match(0, len(unify_tit), 0, len(second_tit[0])))
        unify_tit = unify_tit[match.a: match.a + match.size]
        ser += int(one[1][0])
    res = [unify_tit, str(ser), sco]
    return res

def fran_to_stan(adddict):
    """
    Convert franchise titles to standalone-titles
    """
    copydict = adddict.copy()
    standict = {}
    addlist = []
    first_tit = copydict.popitem()
    for one in copydict.copy():
        second_tit = copydict.popitem()
        if first_tit[1][2] == second_tit[1][2]:
            addlist.append(second_tit)
        else:
            addlist.append(first_tit)
            one = list_seqven(addlist)
            standict[one[0]] = [one[1], one[2]]
            first_tit = second_tit
            addlist.clear()
    addlist.append(first_tit)
    one = list_seqven(addlist)
    standict[one[0]] = [one[1], one[2]]
    return standict


def dict_to_list(anidict):
    """
    List is simplier than dict
    """
    lines = []
    for title, data in anidict.items():
        lines.append([title, data[0], data[1]])
    return lines

def list_to_sort(lines, col_mode=2, rev_mode=True):#mode=1 or mode=2
    """
    Just sort (by series or scores, straight or reversed) and print it
    """
    lines.sort(key=lambda lines: int(lines[col_mode]), reverse=rev_mode)
    print('\n'.join(map(str, lines)))

def list_to_form(lines):
    """
    Some formating staff. Easy to change in the future (TODO?)
    """
    maxani = ''
    maxser = ''
    maxsco = ''
    for one in lines:
        if len(one[0]) > len(maxani):
            maxani = one[0]
        if len(one[1]) > len(maxser):
            maxser = one[1]
        if len(one[2]) > len(maxsco):
            maxsco = one[2]
    maxani += '    '

    for one in lines:
        while len(one[0]) < len(maxani):
            one[0] += ' '
        while len(one[1]) < len(maxser):
            one[1] += ' '
        while len(one[2]) < len(maxsco):
            one[2] += ' '
        one[0] = '| ' + one[0]
        one[0] += '| '
        one[1] += '  | '
        one[2] += '  |'

def form_to_file(lines):
    """
    Finally, writes all data to userfriendly txt file
    """
    with open(path.join(HERE, 'anilist.txt'), 'w') as outf:
        for one in lines:
            for att in one:
                outf.write(att)
            outf.write('\n')

def main():
    """
    The programm's head
    """
    anidict, adddict = xml_to_dict()
    standict = fran_to_stan(adddict)
    anidict.update(standict)
    lines = dict_to_list(anidict)
    list_to_sort(lines, 2, True)
    list_to_form(lines)
    form_to_file(lines)

if __name__ == "__main__":
    main()
